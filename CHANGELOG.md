# Changelog
## [1.1.1] - 2018.05.27
### Fixed
- 404 page

## [1.1] - 2018.05.26
### Added
- Ability to change size status (rows)
- Ability to hide twitter widget
- Ability to change to your customize email and twitter labels and messages

### Changed
- Jekyll version
- Font time tag
- Footer design
- Navbar design
- 404 page
- CSS moved to a separate file
- Design pages
- Layout feed.xml
- Code compression index.html
- Compressed all pages
- And other minor changes

### Removed
- id tags from index
- Zone from config and pages

## [1.0] - 2018.05.17
### Added
- First release

[1.1.1]: https://gitlab.com/tophackr/status/tags/v1.1.1
[1.1]: https://gitlab.com/tophackr/status/tags/v1.1
[1.0]: https://gitlab.com/tophackr/status/tags/v1.0
